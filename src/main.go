package main

import (
	"apis/product_api"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/api/product/findall", product_api.FindAll).Methods("GET")
	router.HandleFunc("/api/product/search/{keyword}", product_api.Search).Methods("GET")
	router.HandleFunc("/api/product/create", product_api.Create).Methods("POST")
	router.HandleFunc("/api/product/update", product_api.Update).Methods("PUT")
    router.HandleFunc("/api/product/delete/{id}", product_api.Delete).Methods("Delete")

	err := http.ListenAndServe(":5000", router)
	if err != nil {
		fmt.Println(err)
	}

}